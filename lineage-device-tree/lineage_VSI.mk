#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from VSI device
$(call inherit-product, device/hmd/VSI/device.mk)

PRODUCT_DEVICE := VSI
PRODUCT_NAME := lineage_VSI
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia C31
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="T19681AA1_Natv-user 12 SP1A.210812.016 00WW_1_180 release-keys"

BUILD_FINGERPRINT := Nokia/Vision_00WW/VSI:12/SP1A.210812.016/00WW_1_180:user/release-keys
