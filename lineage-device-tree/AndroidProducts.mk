#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_VSI.mk

COMMON_LUNCH_CHOICES := \
    lineage_VSI-user \
    lineage_VSI-userdebug \
    lineage_VSI-eng
